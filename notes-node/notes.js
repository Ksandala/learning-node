console.log('Starting notes.js');

const fs = require('fs');
var fetchNotes = () => {
  try{
    var notesData = fs.readFileSync('notes-data.json');
    return JSON.parse(notesData);
    }
    catch (err){
      return [];
    }
};

var saveNotes = (notes) => {
  fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

var addNote = (title, body) => {
  var notes = fetchNotes();
  var note = {
    title,
    body
  };

  var duplicateNote = notes.filter((note) => note.title === title );
  if (duplicateNote.length === 0){
    notes.push(note);
    saveNotes(notes);
    return note;
  } 
};
var getAll = () => {
  var notes = fetchNotes();
  if ( notes.length === 0 ){
    console.log('No Notes saved yet.!')
  }else{
    console.log(notes);
  }
};

var getNote = (title) => {
  var notes = fetchNotes();
  var noteSearched = notes.filter((note) => note.title === title);
  console.log(noteSearched);
};
var removeNote = (title) => {
  var notes = fetchNotes();
  var filterNote = notes.filter((note) => note.title === title);
  if(filterNote.length !== 0){
    notes.pop(filterNote)
    saveNotes(notes);
    return filterNote;
  }
}
module.exports= {
  addNote,
  getAll,
  getNote,
  removeNote
};
