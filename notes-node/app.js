console.log('Starting app.js');

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes.js');

var args = yargs.argv;
var command = process.argv[2];
console.log( `Command: ${command}`);

if( command === 'add'){
    //Adding Note
    var note = notes.addNote(args.title, args.body)
    if(note){ 
        console.log( 'Note saved..!');
        console.log('---');
        console.log(`Title : ${note.title}`);
        console.log(`Body : ${note.body}`);
    }else{
        console.log('Note title already exist, Choose Different one');
    }
}else if(command === 'list'){
    //Listing Notes
    notes.getAll();
}else if(command === 'read'){
    //Reading Note
    notes.getNote(args.title);
}else if(command === 'remove'){
    //Removing Note
    var removedNote = notes.removeNote(args.title);
    if(removedNote){
        console.log('Note Removed');
    }else{
        console.log(`Note not exist with title :${args.title}`)
    }
}else{
    console.log('Command not recognized');
}

